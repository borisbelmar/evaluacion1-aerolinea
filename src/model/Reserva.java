package model;

public class Reserva {
	private String name;
	private int qty;
	private Vuelo vuelo;
	private boolean roundTrip;
	private String date;
	private int days;
	
	public Reserva(String name, int qty, Vuelo vuelo, boolean roundTrip, String date, int days) {
		this.name = name;
		this.qty = qty;
		this.vuelo = vuelo;
		this.roundTrip = roundTrip;
		this.date = date;
		this.days = days;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public Vuelo getVuelo() {
		return vuelo;
	}

	public void setVuelo(Vuelo vuelo) {
		this.vuelo = vuelo;
	}

	public boolean isRoundTrip() {
		return roundTrip;
	}

	public void setRoundTrip(boolean roundTrip) {
		this.roundTrip = roundTrip;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getDays() {
		return days;
	}

	public void setDays(int days) {
		this.days = days;
	}

	public int calculateDiscount() {
		if(this.days >= 30 && this.days <= 60) {
			return this.vuelo.getPrice();
		} else if(this.days >= 61 && this.days <= 90) {
			return (int)Math.round(this.vuelo.getPrice() * 0.9);
		} else if(this.days >= 91 && this.days <= 120) {
			return (int)Math.round(this.vuelo.getPrice() * 0.85);
		} else {
			throw new RuntimeException("Cantidad de días inválida");
		}
	}
	
	public int getDiscount() {
		if(this.days >= 30 && this.days <= 60) {
			return 0;
		} else if(this.days >= 61 && this.days <= 90) {
			return 10;
		} else if(this.days >= 91 && this.days <= 120) {
			return 15;
		} else {
			throw new RuntimeException("Cantidad de días inválida");
		}
	}
	
	public int getTotalByPerson() {
		if(this.roundTrip) {
			return this.calculateDiscount() * 2;
		} else {
			return this.calculateDiscount();
		}
	}
	
	public int getTotalPrice() {
		return this.getTotalByPerson() * this.qty;
	}
}
