package model;

public class Vuelo {
	private String name;
	private int price;
	
	// Constructor
	
	protected Vuelo(String name, int price) {
		this.name = name;
		this.price = price;
	}
	
	// Getters & Setters
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	
	
}
