package model;

public class VueloBuilder {
	public static Vuelo build(int vuelo) {
		switch(vuelo) {
			case 1:
				return new Vuelo("Vuelo 1", 67000);
			case 2:
				return new Vuelo("Vuelo 2", 59000);
			case 3:
				return new Vuelo("Vuelo 3", 90000);
			case 4:
				return new Vuelo("Vuelo 4", 30000);
			case 5:
				return new Vuelo("Vuelo 5", 76000);
			default:
				throw new RuntimeException("Valor inválido");
		}
	}
}
