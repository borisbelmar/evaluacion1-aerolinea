package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Reserva;
import model.Vuelo;
import model.VueloBuilder;

/**
 * Servlet implementation class Calculo
 */
@WebServlet("/Calculo")
public class Calculo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Calculo() {
        super();
    }
    
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
        	String name = request.getParameter("name");
        	int vuelo = Integer.parseInt(request.getParameter("vuelo"));
        	int qty = Integer.parseInt(request.getParameter("qty"));
        	boolean roundTrip = request.getParameter("roundTrip") != null ? true : false;
        	String reservaDate = request.getParameter("reservaDate");
        	int days = Integer.parseInt(request.getParameter("days"));
        	
        	Vuelo vueloObj = VueloBuilder.build(vuelo);
        	
        	Reserva reserva = new Reserva(name, qty, vueloObj, roundTrip, reservaDate, days);
            
            request.setAttribute("reserva", reserva);
            
            this.getServletContext().getRequestDispatcher("/reserva.jsp").forward(request, response);
        	
        }
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

}
