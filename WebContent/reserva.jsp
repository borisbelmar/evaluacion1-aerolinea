<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<% model.Reserva reserva = (model.Reserva)request.getAttribute("reserva"); %>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="includes/head.html" />
</head>
<body>
    <section class="mt-5">
        <div class="container">
            <h1 class="mb-5">Gracias por reservar!</h1>
            <div class="row">
                <div class="col-12 col-md-4 mt-3">
                    <div class="card">
                        <div class="card-header">
                            Datos de la Compra
                        </div>
                        <div class="card-body">
                            <p><strong>Nombre: </strong> <%= reserva.getName() %></p>
                            <p><strong>Vuelo: </strong> <%= reserva.getVuelo().getName() %></p>
                            <p><strong>Precio: </strong> $<%= reserva.getVuelo().getPrice() %></p>
                            <p><strong>Fecha: </strong> <%= reserva.getDate() %></p>
                            <p><strong>¿Ida y Vuelta?: </strong><%= reserva.isRoundTrip() ? "Si" : "No" %></p>
                        </div>
                    </div>
                    <a href="./" class="btn btn-primary btn-block mt-3">
                        Volver
                    </a>
                </div>
                <div class="col-12 col-md-8 mt-3">
                    <div class="card">
                        <div class="card-header">
                            Detalle de la compra
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                    <th>Total por Boleto</th>
                                    <th>Descuento</th>
                                    <th>Total por persona</th>
                                    <th>Cantidad</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>$<%= reserva.calculateDiscount() %></td>
                                        <td><%= reserva.getDiscount() %>%</td>
                                        <td>$<%= reserva.getTotalByPerson() %></td>
                                        <td><%= reserva.getQty() %></td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Total Final</th>
                                        <th colspan="3">$<%= reserva.getTotalPrice() %></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <jsp:include page="includes/footer.html" />
</body>
</html>