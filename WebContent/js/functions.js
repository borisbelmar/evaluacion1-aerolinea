function elementById(id) {
    return document.getElementById(id);
}

function validate() {
	if(validateDays()) {
		return true;
	} else {
		return false;
	}
}

function validateDays() {
    var todayDate = moment(new Date());
    console.log(elementById('reservaDate').value);
	var reservaDate = moment(elementById('reservaDate').value);
	var days = reservaDate.diff(todayDate, 'days');
	if(days >= 30 && days <= 120) {
		elementById('days').value = days;
		elementById('reservaDate').classList.remove('is-invalid');
		elementById('reservaDateHelp').classList.replace('text-danger', 'text-muted');
		return true;
	} else {
		elementById('reservaDate').classList.add('is-invalid');
		elementById('reservaDateHelp').classList.replace('text-muted', 'text-danger');
		return false;
	}
}