<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="includes/head.html" />
</head>
<body>
    <div class="container my-5">
        <h1>Gran oferta a Buenos Aires!</h1>
        <h3>Vuelos a la ciudad de la furia a precio huevo</h3>
        <div class="row">
            <div class="col-12 col-md-8 mt-5">
                <div class="card">
                    <div class="card-body">
                        <form action="Calculo" method="POST" onsubmit="return validate()">
                            <div class="form-group">
                                <label for="name">Nombre Completo</label>
                                <input type="text" class="form-control" name="name" placeholder="Ingresa tu nombre" required>
                            </div>
                            <div class="form-group">
                                <label for="vuelo">¿Qué vuelo deseas reservar?</label>
                                <select class="form-control" name="vuelo" required>
                                    <option selected disabled>Selecciona una opción</option>
                                    <option value="1">Vuelo 1 - $67.000</option>
                                    <option value="2">Vuelo 2 - $59.000</option>
                                    <option value="3">Vuelo 3 - $90.000</option>
                                    <option value="4">Vuelo 4 - $30.000</option>
                                    <option value="5">Vuelo 5 - $76.000</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="qty">Cantidad de personas</label>
                                <select class="form-control" name="qty" required>
                                    <option value="1" selected>1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="true" name="roundTrip">
                                    <label class="form-check-label" for="roundTrip">
                                    ¿Es ida y vuelta?
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="reservaDate">Ingrese el día de la reserva</label>
                                <input type="date" name="reservaDate" class="form-control" id="reservaDate" onfocusout="validateDays()" required>
                                <small id="reservaDateHelp" class="form-text text-muted">La fecha debe ser mínimo 30 días después de la fecha actual y hasta 120 días.</small>
                            </div>
                            <input type="text" id="days" name="days" hidden required>
                            <input type="submit" class="btn btn-primary btn-block" value="Enviar">
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4 mt-5">
                <div class="card">
                    <div class="card-body">
                        <h5>Tabla de descuentos</h5>
                        <table class="table">
                            <thead>
                                <th>Días de anticipación</th>
                                <th>Descuento</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>30 a 60</td>
                                    <td>0%</td>
                                </tr>
                                <tr>
                                    <td>61 a 90</td>
                                    <td>10%</td>
                                </tr>
                                <tr>
                                    <td>91 a 120</td>
                                    <td>15%</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    <jsp:include page="includes/footer.html" />
</body>
</html>